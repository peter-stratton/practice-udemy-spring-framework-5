package guru.springframework.spring5webapp.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by pstratton on 8/29/17.
 */
public class Author {

    private String firstName;
    private String lastName;

    private Set<Book> books = new HashSet<>();



    private Author() {

    }

    public Author(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Author(String firstName, String lastName, Set<Book> books) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.books = books;
    }

    public String getFirstName() { return firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public Set<Book> getBooks() { return books; }

    public void setBooks(Set<Book> books) { this.books = books; }
}
